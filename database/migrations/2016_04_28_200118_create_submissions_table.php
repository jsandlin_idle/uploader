<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submissions', function (Blueprint $table) {
            $table->increments('id');

            // Agreement infos
            // * Can these all be one thing?
            $table->boolean('on_air')->nullable();
            $table->boolean('on_stream')->nullable();
            $table->boolean('on_facebook')->nullable();
            $table->boolean('on_youtube')->nullable();

            // artist info
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('address');

            // track info
            $table->string('track_artist');
            $table->string('track_name');
            $table->string('track_length');
            $table->string('video_director');
            $table->string('record_label');
            $table->date('release_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('submissions');
    }
}
