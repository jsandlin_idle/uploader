<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Submission;
use Storage;
use Carbon\Carbon;

class Curate extends Controller
{
    public function index()
    {
        $submissions = Submission::all();
        return view('curate.index', compact('submissions'));
    }

    public function expiring()
    {
        $submissions = Submission::where('created_at', '<=', Carbon::now()->subDays(80))->get();
        return view('curate.index', compact('submissions'));
    }

    public function recent()
    {
    }

    public function show($id)
    {
        $submission = Submission::find($id);
        return view('curate.show', compact('submission'));
    }

    public function edit($id)
    {
        $submission = Submission::find($id);
        return view('curate.edit', compact('submission'));
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
        $submission = Submission::find($id);
        $submission->delete();
        return redirect()->route('admin.index');
    }
}
