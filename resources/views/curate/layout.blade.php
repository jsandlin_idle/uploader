<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Adult Swim Music Video Submission - Admin</title>
</head>
<body>
    <div class="container">
        <div class="page-header">
            <h1>Adult Swim Music Video Submission - Admin</h1>
        </div>

        <div class="col-md-4">
            <div class="panel panel-default">
                <ul class="list-group">
                    <li class="list-group-item">{{ link_to_route('admin.index', 'All Submissions') }}</li>
                    <li class="list-group-item">{{ link_to_route('expiring', 'Expiring Soon') }}</li>
                </ul>
            </div>
        </div>

        <div class="col-md-8">
            @yield('content')
        </div>

    </div>
</body>
</html>
