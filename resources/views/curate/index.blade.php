@extends('curate.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Submissions</div>
        <div class="panel-body">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Submitted On</th>
                        <th>Track Title</th>
                        <th>Artist / Label</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($submissions as $submission)
                    <tr class="{{$submission->isExpiringSoon() ? 'danger' : ''}}">
                        <th scope="row">{{$submission->id}}</th>
                        <td>{{$submission->created_at}}</td>
                        <td>{{$submission->track_name}}</td>
                        <td>{{$submission->first_name}} {{$submission->last_name}} / {{$submission->record_label}}</td>
                        <td>{{link_to_route('admin.show', 'View', ['id' => $submission->id], ['class' => 'btn btn-sm btn-primary'])}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection
