<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', "Submission@create");
Route::post('/', "Submission@store");

Route::get('admin/expiring', 'Curate@expiring')->name('expiring');
Route::resource('admin', 'Curate');

Route::auth();

Route::get('/home', 'HomeController@index');
