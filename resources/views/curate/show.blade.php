@extends('curate.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Submission</div>
        <div class="panel-body">
            <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="info">
                            <td></td>
                            <th scope="row">Submission Info</th>
                        </tr>

                        <tr>
                            <th scope="row">Submitted on</th>
                            <td>{{$submission->created_at}}</td>
                        </tr>

                        <tr>
                            <th scope="row">Expires On</th>
                            <td>{{$submission->created_at->addDays(90)}}</td>
                        </tr>

                        <tr class="info">
                            <td></td>
                            <th scope="row">Artist Information</th>
                        </tr>

                        <tr>
                            <th scope="row">Artist Name</th>
                            <td>{{$submission->first_name}} {{$submission->last_name}}</td>
                        </tr>

                        <tr>
                            <th scope="row">Artist Email</th>
                            <td>{{$submission->email}}</td>
                        </tr>

                        <tr>
                            <th scope="row">Artist Address</th>
                            <td>{{$submission->address}}</td>
                        </tr>

                        <tr class="info">
                            <td></td>
                            <th scope="row">Track Information</th>
                        </tr>

                        <tr>
                            <th scope="row">Track URL</th>
                            @if($submission->hasMedia())
                                <td><a href="{{$submission->getMedia()[0]->getUrl()}}">{{$submission->getMedia()[0]->getUrl()}}</a></td>
                            @endif
                        </tr>

                        <tr>
                            <th scope="row">Track Name</th>
                            <td>{{$submission->track_name}}</td>
                        </tr>

                        <tr>
                            <th scope="row">Track Length</th>
                            <td>{{$submission->track_length}}</td>
                        </tr>

                        <tr>
                            <th scope="row">Video Director</th>
                            <td>{{$submission->video_director}}</td>
                        </tr>

                        <tr>
                            <th scope="row">Record Label</th>
                            <td>{{$submission->record_label}}</td>
                        </tr>

                        <tr class="info">
                            <td></td>
                            <th scope="row">Release</th>
                        </tr>

                        <tr>
                            <th scope="row">On Air</th>
                            <td>{{$submission->on_air}}</td>
                        </tr>

                        <tr>
                            <th scope="row">On Stream</th>
                            <td>{{$submission->on_stream}}</td>
                        </tr>

                        <tr>
                            <th scope="row">On Facebook</th>
                            <td>{{$submission->on_facebook}}</td>
                        </tr>

                        <tr>
                            <th scope="row">On Youtube</th>
                            <td>{{$submission->on_youtube}}</td>
                        </tr>

                        <tr>
                            <th scope="row">Preferred Release Date</th>
                            <td>{{$submission->release_date}}</td>
                        </tr>
                    </tbody>
            </table>
            {{ link_to_route('admin.index', 'Back', null, ['class' => 'btn btn-primary pull-left'] )}}

            {{ Form::open(['route' => ['admin.destroy', $submission->id], 'method' => 'delete']) }}
                {{ Form::submit('Delete', ['class' => 'btn btn-danger pull-right']) }}
            {{ Form::close() }}


        </div>
    </div>
@endsection
