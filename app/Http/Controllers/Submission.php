<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Submission;
use Storage;

class Submission extends Controller
{
    public function create()
    {
        return view('submissions.index');
    }

    public function store(Request $request)
    {
        $submission = new Submission();
        // agreement info
        $submission->on_air = $request->input('on_air');
        $submission->on_stream = $request->input('on_stream');
        $submission->on_facebook = $request->input('on_facebook');
        $submission->on_youtube = $request->input('on_youtube');

        // artist contact info
        $submission->first_name = $request->input('fname');
        $submission->last_name = $request->input('lname');
        $submission->email = $request->input('email');
        $submission->address = $request->input('mailing');

        // track infos
        $submission->track_artist = $request->input('track_artist');
        $submission->track_name = $request->input('track_name');
        $submission->track_length = $request->input('track_length');
        $submission->video_director = $request->input('track_director');
        $submission->record_label = $request->input('track_label');
        $submission->release_date = $request->input('release_preference');

        // have to save it before adding media to it
        $submission->save();
        $submission->addMediaFromRequest('upload')->toCollectionOnDisk('media', 's3');

        // Save once more with new associations
        $submission->save();

        // now need to trigger queue job for sending email

        return view('submissions.thanks');
    }
}
