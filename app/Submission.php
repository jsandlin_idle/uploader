<?php

namespace App;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Submission extends Model implements HasMedia
{
    use HasMediaTrait;

    public function isExpiringSoon() {
        $notify_date = $this->created_at->addDays(80);
        $now = Carbon::now();
        return $now->gte($notify_date);
    }
}
